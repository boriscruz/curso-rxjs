import { Observable, Observer, interval, Subject } from "rxjs";


const observer: Observer<any> = {
    next: value => console.log('Next: ', value),
    error: error => console.log('Error: ', error),
    complete: () => console.log('Completed')
}


const interval$ = new Observable<number>(subscriber => {
    const intervalID = setInterval(()=>subscriber.next(Math.random()),5000);
    return () => clearInterval(intervalID);
});

// const subs1 = interval$.subscribe(random => console.log('subs1 ',random));
// const subs2 = interval$.subscribe(random => console.log('subs2 ',random));

const subject$ = new Subject();

interval$.subscribe(subject$);

const subs1 = subject$.subscribe(random => console.log('subs1 ',random));
const subs2 = subject$.subscribe(random => console.log('subs2 ',random));