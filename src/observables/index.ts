import { Observable, Observer, interval } from "rxjs";


const observer: Observer<any> = {
    next: value => console.log('Next: ', value),
    error: error => console.log('Error: ', error),
    complete: () => console.log('Completed')
}


const interval$ = new Observable<number>(subscriber => {

    let count: number = 0;

    const interval = setInterval(() => {
        count++;
        subscriber.next(count);
    }, 5000);

    return () => {
        clearInterval(interval);
        console.log('Destroyed intervalo');

    }

});

const subs = interval$.subscribe(num => console.log('Num: ', num));

setTimeout(() => {
    subs.unsubscribe();
    console.log('timeout completed');

}, 10000);
